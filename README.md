# google-oss_arch

IaC definitions for three of the standard Puppet OSS architectures for Google Cloud Platform

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with google-puppet_oss_arch](#setup)
    * [What google-puppet_oss_arch affects](#what-google-puppet_oss_arch-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with google-puppet_oss_arch](#beginning-with-google-puppet_oss_arch)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Limitations - OS compatibility, etc.](#limitations)
5. [Development - Guide for contributing to the module](#development)

## Description

This Terraform module implements as code the infrastructure required to deploy three permutations of the Puppet OSS architectures: Standard, Large, and Extra Large, addtionally all architectures can have additional infrastructure provisioned to support a failover replica on Google Cloud Platform. This module is developed to function independently but it is often used in support of [spidersddd/possadm](https://gitlab.com/spidersddd/possadm), brought together by [spidersddd/osscdm(https://gitlab.com/spidersddd/osscdm) to facilitate the end-to-end deployment of fully functional stacks of Puppet OSS.

### Expectations and support

NONE

## Setup

### What google-puppet_oss_arch affects

Types of things you'll be paying your cloud provider for

* Instances of various sizes
* Load balancers
* Networks

### Setup Requirements

* [GCP Cloud SDK Intalled](https://cloud.google.com/sdk/docs/quickstarts)
* [GCP Application Default Credentials](https://cloud.google.com/sdk/gcloud/reference/auth/application-default/)
* [Git Installed](https://git-scm.com/downloads)
* [Terraform (>= 0.12.20) Installed](https://www.terraform.io/downloads.html)

### Beginning with google-puppet_oss_arch

1. Clone this repository
    * `git clone https://gitlab.com/spidersddd/terraform-google-puppet_oss_arch.git && cd terraform-google-puppet_oss_arch`
2. Install module dependencies: `terraform init`
3. Initiate plan for the default standard architecture
    * `terraform apply -auto-approve -var "project=example.com" -var "user=john.doe" -var "firewall_allow=[ \"0.0.0.0/0\" ]"`
4. Moments later you'll be presented with a single VM where to install Puppet OSS

## Usage

### Example: deploy large architecture with replica and a more restrictive network

This will give you the absolute minimum needed for installing Puppet OSS, a single VM plus a specific network for it to reside within and limited to a specific network that have access to the new infrastructure (note: internal network will always be injected into the list)

`terraform apply -auto-approve -var "project=example.com" -var "user=john.doe" -var "firewall_allow=[ \"192.69.65.0/24\" ]" -var "architecture=large" -var "replica=true"`

### Example: destroy stack

The number of options required are reduced when destroying a stack

`terraform destroy -auto-approve -var "project=example.com" -var "user=john.doe"`

## Usage notes

1. For making ssh access work with Terraform's google provider, you will need to add your private key corresponding to the public key in the `ssh_key` parameter to the ssh agent like so:

```bash
> eval `ssh-agent`
> ssh-add <private_key_path>
```

## Limitations

Currently limited to CentOS and VM disk sizes are not configurable
