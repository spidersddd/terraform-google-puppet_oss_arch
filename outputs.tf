# Output data used by Bolt to do further work, doing this allows for a clean and
# abstracted interface between cloud provider implementations
output "ca-address" {
  value       = module.instances.console
  description = "This will by the external IP address assigned to the Puppet CA"
}
output "pool" {
  value       = module.loadbalancer.lb_dns_name
  description = "The GCP internal network FQDN of the Puppet compiler pool"
  sensitive   = true
}
